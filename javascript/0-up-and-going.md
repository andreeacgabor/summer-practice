# Up & going

## Into Programming

- Code
  - Statements
- Expressions
  - Executing a Program
- Try It Yourself
  - Output
  - Input
- Operators
- Values & Types
  - Converting Between Types
- Code Comments
- Variables
- Blocks
- Conditionals
- Loops
- Functions
  - Scope
- Practice

## Into JavaScript

- Values & Types
  - Objects
  - Built-In Type Methods
  - Comparing Values
- Variables
  - Function Scopes
- Conditionals
- Strict Mode
- Functions as Values
  - Immediately Invoked Function Expressions (IIFEs)
  - Closure
- this Identifier
- Prototypes
- Old & New
  - Polyfilling
  - Transpiling
- Non-JavaScript

## Into YDKJS

- Scope & Closures
- this & Object Prototypes
- Types & Grammar
- Async & Performance
- ES6 & Beyond
