# Scope & Closure

## What Is Scope?

- Compiler Theory
- Understanding Scope
  - The Cast
  - Back and Forth
  - Compiler Speak
  - Engine/Scope Conversation
  - Quiz
- Nested Scope
  - Building on Metaphors
- Errors

## Lexical Scope

- Lex-time
  - Look-ups
- Cheating Lexical
  - eval
  - with
  - Performance

## Function Versus Block Scope

- Scope From Functions
- Hiding in Plain Scope
  - Collision Avoidance
- Functions as Scopes
  - Anonymous Versus Named
  - Invoking Function Expressions Immediately
- Blocks as Scopes
  - with
  - try/catch
  - let
  - const

## Hoisting

- Chicken or the Egg?
- The Compiler Strikes Again
- Functions First

## Scope Closure

- Enlightenment
- Nitty Gritty
- Now I Can See
- Loops and Closure
  - Block Scoping Revisited
- Modules
  - Modern Modules
  - Future Modules

## Dynamic Scope

## Polyfilling Block Scope

- Traceur
- Implicit Versus Explicit Blocks
- Performance

## Lexical This
