# ES6 & Beyond

## ES? Now & Future

- Versioning
- Transpiling
  - Shims / Polyfils

## Syntax

- Block-Scoped Declarations
  - let Declarations
  - const Declarations
  - Block-Scoped Functions
- Spread/Rest
- Default Parameter Values
  - Default Value Expressions
- Destructuring
  - Object Property Assignment Pattern
  - Not Just Declarations
  - Repeated Assignments
  - Too Many, Too Few, Just Enough
  - Default Value Assignment
  - Nested Destructuring
  - Destructuring Parameters
- Object Literal Extensions
  - Concise Properties
  - Concise Methods
  - Computed Property Names
  - Setting \[\[Prototype\]\]
  - Object super
- Template Literals
  - Interpolated Expressions
  - Tagged Template Literals
- Arrow Functions
  - Not Just Shorter Syntax, But this
- for..of Loops
- Regular Expressions
  - Unicode Flag
  - Sticky Flag
  - Regular Expression flags
- Number Literal Extensions
- Unicode
  - Unicode-Aware String Operations
  - Character Positioning
  - Unicode Identifier Names
- Symbols
  - Symbol Registry
  - Symbols as Object Properties

## Organization

- Iterators
  - Interfaces
  - next() Iteration
  - Optional: return(..) and throw(..)
  - Iterator Loop
  - Custom Iterators
  - Iterator Consumption
- Generators
  - Syntax
  - Iterator Control
  - Early Completion
  - Error Handling
  - Transpiling a Generator
  - Generator Uses
- Modules
  - The Old Way
  - Moving Forward
  - The New Way
  - Circular Module Dependency
  - Module Loading
- Classes
  - class
  - extends and super
  - new.target
  - static

## Async Flow Control

- Promises
  - Making and Using Promises
  - Thenables
  - Promise API
- Generators + Promises

## Collections

- TypedArrays
  - Endianness
  - Multiple Views
  - Typed Array Constructors
- Maps
  - Map Values
  - Map Keys
- WeakMaps
- Sets
  - Set Iterators
- WeakSets

## API Additions

- Array
  - Array.of(..) Static Function
  - Array.from(..) Static Function
  - Creating Arrays and Subtypes
  - copyWithin(..) Prototype Method
  - fill(..) Prototype Method
  - find(..) Prototype Method
  - findIndex(..) Prototype Method
  - entries(), values(), keys() Prototype Methods
- Object
  - Object.is(..) Static Function
  - Object.getOwnPropertySymbols(..) Static Function
  - Object.setPrototypeOf(..) Static Function
  - Object.assign(..) Static Function
- Math
- Number
  - Static Properties
  - Number.isNaN(..) Static Function
  - Number.isFinite(..) Static Function
  - Integer-Related Static Functions
- String
  - Unicode Functions
  - String.raw(..) Static Function
  - repeat(..) Prototype Function
  - String Inspection Functions

## Meta Programming

- Function Names
  - Inferences
- Meta Properties
  - Well-Known Symbols
  - Symbol.iterator
  - Symbol.toStringTag and Symbol.hasInstance
  - Symbol.species
  - Symbol.toPrimitive
  - Regular Expression Symbols
  - Symbol.isConcatSpreadable
  - Symbol.unscopables
- Proxies
  - Proxy Limitations
  - Revocable Proxies
  - Using Proxies
- Reflect API
  - Property Ordering
- Feature Testing
  - FeatureTests.io
- Tail Call Optimization (TCO)
  - Tail Call Rewrite
  - Non-TCO Optimizations
  - Meta?

## Beyond ES6

- async functions
  - Caveats
- Object.observe(..)
  - Custom Change Events
  - Ending Observation
- Exponentiation Operator
- Objects Properties and ...
- Array#includes(..)
- SIMD
- WebAssembly (WASM)
